import { Component } from '@angular/core';

@Component({
  selector: 'test-nx-build-and-release-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'app';
}
