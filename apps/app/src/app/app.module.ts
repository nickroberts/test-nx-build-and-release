import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Lib1Module } from '@test/lib1';
import { Lib4Child1Module } from '@test/lib4/lib4-child1';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, Lib1Module, Lib4Child1Module],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
