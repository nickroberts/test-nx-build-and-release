#!/usr/bin/env node
const { execSync } = require('child_process');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const argv = yargs(hideBin(process.argv)).argv;

const { branch, tag } = argv;

if (
  (!branch || typeof branch !== 'string') &&
  (!tag || typeof tag !== 'string')
) {
  console.error('\nERROR: Branch or tag is required.');
  process.exit(9);
}

if (tag) {
  execSync(`node tools/scripts/generate-config.js --branch latest`, {
    stdio: ['pipe', process.stdout, process.stderr],
  });
} else if (branch === 'main' || branch === 'release') {
  const cmd =
    branch === 'main'
      ? 'npx nx run-many --target=test --all --codeCoverage && node tools/scripts/combine-coverage.js'
      : 'npx nx run-many --target=test --all';
  execSync(cmd, { stdio: ['pipe', process.stdout, process.stderr] });
  execSync(`node tools/scripts/generate-config.js --branch ${branch}`, {
    stdio: ['pipe', process.stdout, process.stderr],
  });
} else {
  execSync('npx nx affected:test --base=origin/main', {
    stdio: ['pipe', process.stdout, process.stderr],
  });
}
