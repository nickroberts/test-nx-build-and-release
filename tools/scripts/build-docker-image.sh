#!/bin/sh

if [ $# -ne 2 ]; then
  echo 1>&2 "Usage: $0 PROJECT TAG"
  exit 1
fi

PROJECT=$1
TAG=$2

# PACKAGE_VERSION=$(cat package.json|grep version|head -1|awk -F: '{ print $2 }'|sed 's/[", ]//g')
# echo "PACKAGE_VERSION: ${PACKAGE_VERSION}"

case $TAG in
  latest)
    VERSION=$CI_COMMIT_TAG
    ;;
  *)
    VERSION=$TAG.$CI_COMMIT_SHORT_SHA
    ;;
esac

echo "Building docker image $CI_REGISTRY_IMAGE/$PROJECT:$VERSION with additional tag ${TAG}"

# /kaniko/executor \
#   --context $CI_PROJECT_DIR \
#   --cache=true \
#   --dockerfile $CI_PROJECT_DIR/apps/$PROJECT/Dockerfile \
#   --destination $CI_REGISTRY_IMAGE/$PROJECT:$VERSION \
#   --destination $CI_REGISTRY_IMAGE/$PROJECT:$TAG
