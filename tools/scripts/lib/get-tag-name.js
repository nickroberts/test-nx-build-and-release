const getTagName = (branch) => {
  switch (branch) {
    case 'main':
      return 'dev';
    case 'release':
      return 'next';
    default:
      return 'latest';
  }
};

module.exports = { getTagName };
