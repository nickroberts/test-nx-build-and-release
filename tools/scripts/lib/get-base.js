const getBase = (branch) => {
  let base;
  switch (branch) {
    case 'main':
      base = `origin/main~1`;
      break;
    case 'release':
    case 'latest':
    default:
      base = 'origin/main';
  }
  return base;
};

module.exports = { getBase };
