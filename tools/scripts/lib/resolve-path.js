const { resolve } = require('path');

const resolvePath = (path) => resolve(__dirname, `../../../${path}`);

module.exports = { resolvePath };
