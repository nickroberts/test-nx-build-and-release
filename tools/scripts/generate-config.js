#!/usr/bin/env node
const { execSync } = require('child_process');
const { writeFileSync } = require('fs');
const { dump } = require('js-yaml');
const yargs = require('yargs/yargs');
const { hideBin } = require('yargs/helpers');
const { getBase } = require('./lib/get-base');
const { getTagName } = require('./lib/get-tag-name');
const { resolvePath } = require('./lib/resolve-path');
const argv = yargs(hideBin(process.argv)).argv;

const { branch } = argv;

if (!branch || typeof branch !== 'string') {
  console.error('\nERROR: Branch is required.');
  process.exit(9);
}

const json = {};

const base = getBase(branch);

const getAffectedCmd = (type) =>
  `npx nx affected:${type} --base=${base} --target=build --plain`;

const affectedApps = execSync(getAffectedCmd('apps'))
  .toString()
  .trim()
  .split(' ');

const affectedLibs = execSync(getAffectedCmd('libs'))
  .toString()
  .trim()
  .split(' ');

const tagName = getTagName(branch);

if (affectedLibs.length || affectedApps.length) {
  json[`build`] = {
    extends: ['.node-template'],
    stage: 'build',
    before_script: ['npm ci --cache .npm --prefer-offline'],
    script: [
      `npx nx run-many --target=build --projects=${[
        ...affectedLibs,
        ...affectedApps,
      ].join(',')} --prod --with-deps`,
    ],
    artifacts: {
      paths: ['dist/'],
      expire_in: '1 week',
    },
  };

  if (affectedApps.length) {
    affectedApps.forEach((app) => {
      json[`publish:${app}`] = {
        extends: ['.docker-build-template'],
        stage: 'publish',
        script: [`sh tools/scripts/build-docker-image.sh ${app} ${tagName}`],
      };

      if (tagName === 'latest') {
        json[`deploy:ppd:${app}`] = {
          extends: ['.node-template'],
          stage: 'deploy:dev',
          needs: [{ job: `publish:${app}` }],
          script: [`echo "deploy:ppd:${app} ${tagName}"`],
        };

        json[`deploy:prd:${app}`] = {
          extends: ['.node-template'],
          stage: 'deploy:dev',
          needs: [{ job: `publish:${app}` }],
          script: [`echo "deploy:prd:${app} ${tagName}"`],
          when: 'manual',
        };
      } else if (branch === 'main') {
        json[`deploy:dev:${app}`] = {
          extends: ['.node-template'],
          stage: 'deploy:dev',
          needs: [{ job: `publish:${app}` }],
          script: [`echo "deploy:dev:${app} ${tagName}"`],
        };

        json[`deploy:qa:${app}`] = {
          extends: ['.node-template'],
          stage: 'deploy:qa',
          needs: [
            { job: 'promote:qa:test' },
            { job: 'promote:qa:anchore' },
            { job: 'promote:qa:whitesource' },
          ],
          script: [`echo "deploy:qa:${app} ${tagName}"`],
        };
      }
    });

    if (tagName === 'dev') {
      json['promote:qa:test'] = {
        extends: ['.node-template'],
        stage: 'promote:qa',
        script: [`echo "promote:qa:test ${tagName}"`],
      };
      json['promote:qa:anchore'] = {
        extends: ['.node-template'],
        stage: 'promote:qa',
        script: [`echo "promote:qa:anchore ${tagName}"`],
      };
      json['promote:qa:whitesource'] = {
        extends: ['.node-template'],
        stage: 'promote:qa',
        script: [`echo "promote:qa:whitesource ${tagName}"`],
      };
    }
  }

  affectedLibs.forEach((lib) => {
    json[`publish:${lib}`] = {
      extends: ['.node-template'],
      stage: 'publish',
      needs: [{ job: `build`, artifacts: true }],
      script: [`echo "publish:${lib} ${tagName}"`],
    };
  });
} else {
  json[`build`] = {
    extends: ['.node-template'],
    stage: 'build',
    script: [`echo "Nothing to build!"`],
  };
}

const yml = dump(json, { forceQuotes: true });

writeFileSync(resolvePath('generated-config.yml'), yml);
