const fs = require('fs-extra');
const glob = require('glob');
const { resolve } = require('path');
const libReport = require('istanbul-lib-report');
const reports = require('istanbul-reports');
const libCoverage = require('istanbul-lib-coverage');

const rootDir = resolve(__dirname, '../../coverage');
const reportOut = `${rootDir}/report`;
const coverageFilePath = `${rootDir}/**/coverage-final.json`;

const normalizeJestCoverage = (obj) => {
  const result = { ...obj };
  Object.entries(result)
    .filter(([k, v]) => v.data)
    .forEach(([k, v]) => {
      result[k] = v.data;
    });
  return result;
};

const mergeAllReports = (reports) => {
  const coverageMap = libCoverage.createCoverageMap({});
  if (Array.isArray(reports)) {
    reports.forEach((reportFile) => {
      const coverageReport = fs.readJSONSync(reportFile);
      coverageMap.merge(normalizeJestCoverage(coverageReport));
    });
  }
  return coverageMap;
};

const findAllCoverageReports = (path, callback) => {
  glob(path, {}, (err, reports) => {
    callback(reports, err);
  });
};

const generateReport = (
  coverageMap,
  types = ['html', 'json', 'lcov', 'text']
) => {
  const context = libReport.createContext({
    dir: reportOut,
    coverageMap,
  });
  types.forEach((t) => {
    const report = reports.create(t);
    report.execute(context);
  });
};

async function main() {
  findAllCoverageReports(coverageFilePath, (reports, err) => {
    if (Array.isArray(reports)) {
      generateReport(mergeAllReports(reports), [
        'text-summary',
        'html',
        'json',
        'lcov',
      ]);
    }
  });
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
