import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Lib3Component } from './lib3/lib3.component';

@NgModule({
  imports: [CommonModule],
  declarations: [Lib3Component],
  exports: [Lib3Component],
})
export class Lib3Module {}
