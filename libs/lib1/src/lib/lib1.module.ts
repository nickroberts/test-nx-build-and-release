import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Lib1Component } from './lib1/lib1.component';
import { Lib2Module } from '@test/lib2';

@NgModule({
  imports: [CommonModule, Lib2Module],
  declarations: [Lib1Component],
  exports: [Lib1Component],
})
export class Lib1Module {}
