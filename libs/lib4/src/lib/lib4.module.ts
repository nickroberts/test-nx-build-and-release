import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule],
})
export class Lib4Module {}
// import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
// import { Lib4Child1Module } from '@test/lib4/lib4-child1';
// import { Lib4Child2Module } from '@test/lib4/lib4-child2';
// import { Lib4Child3Module } from '@test/lib4/lib4-child3';

// @NgModule({
//   imports: [CommonModule, Lib4Child1Module, Lib4Child2Module, Lib4Child3Module],
// })
// export class Lib4Module {}
