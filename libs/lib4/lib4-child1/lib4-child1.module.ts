import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Lib4Child1Component } from './lib4-child1/lib4-child1.component';



@NgModule({
  declarations: [Lib4Child1Component],
  imports: [
    CommonModule
  ],
  exports: [Lib4Child1Component]
})
export class Lib4Child1Module { }
