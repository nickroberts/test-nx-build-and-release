import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Lib4Child1Component } from './lib4-child1.component';

describe('Lib4Child1Component', () => {
  let component: Lib4Child1Component;
  let fixture: ComponentFixture<Lib4Child1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Lib4Child1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Lib4Child1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
