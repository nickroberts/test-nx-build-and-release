import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Lib4Child2Component } from './lib4-child2.component';

describe('Lib4Child2Component', () => {
  let component: Lib4Child2Component;
  let fixture: ComponentFixture<Lib4Child2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Lib4Child2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Lib4Child2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
