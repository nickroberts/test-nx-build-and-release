import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Lib4Child2Component } from './lib4-child2/lib4-child2.component';



@NgModule({
  declarations: [Lib4Child2Component],
  imports: [
    CommonModule
  ],
  exports: [Lib4Child2Component]
})
export class Lib4Child2Module { }
