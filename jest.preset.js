const nxPreset = require('@nrwl/jest/preset');

module.exports = {
  ...nxPreset,
  coverageReporters: ['html', 'json', 'lcov', 'text'],
  testPathIgnorePatterns: ['/node_modules/', '.mock.spec.ts$'],
};
