NOCOLOR=\033[0m
GREEN=\033[0;32m

help:
	@echo ""
	@echo "Usage: ${GREEN}make COMMAND${NOCOLOR}"
	@echo ""
	@echo "Commands:"
	@echo ""
	@echo "  ${GREEN}build-app${NOCOLOR}          Builds the test-nx-build-and-release/app container."
	@echo ""

build-app:
	docker build -f apps/app/Dockerfile -t test-nx-build-and-release/app .

.PHONY: help

.DEFAULT:
	@echo "No target for \"$@\". Run \"make help\" for a list of commands."
	@make help
